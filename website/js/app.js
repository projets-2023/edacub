
/**
 * modules utilisés :
 * * d3-select
 * * d3-dsv
 * * d3-array
 * * d3-scale
 */

// Appelle les fonctions de tracé de log après chargement du DOM
document.addEventListener("DOMContentLoaded", displayLogTRH);
document.addEventListener("DOMContentLoaded", displayLogShocks);

var endDate; // date de la dernière acquisition de T/HR

async function displayLogTRH() {
    // Définir les marges, largeur et hauteur de la courbe
    const margin = { top: 20, right: 120, bottom: 30, left: 60 };
    const width = document.getElementById("chart-thr").offsetWidth;
    const height = 300 - margin.top - margin.bottom;

    // Définir les échelles des différents axes
    const xTime = d3.scaleTime()
        .range([0, width]);

    const yTemperature = d3.scaleLinear()
        .range([height, 0]);

    const yHumidity = d3.scaleLinear()
        .range([height, 0]);

    // Définir le contenu des courbes
    const lineTemperature = d3.line()
        .x(d => xTime(d.ts))
        .y(d => yTemperature(d.T))
        .curve(d3.curveMonotoneX)
        ;

    const lineHumidity = d3.line()
        .x(d => xTime(d.ts))
        .y(d => yHumidity(d.HR))
        .curve(d3.curveMonotoneX);

    // Ajouter le SVG dans le document et définir ses dimensions et son emplacement
    const svg = d3.select("#chart-thr").append("svg")
        .attr("id", "svg")
//        .attr("width", width + margin.left + margin.right)
//        .attr("height", height + margin.top + margin.bottom)
//        .attr("viewBox", [0, 0, width, 2*height])
        .attr("width", width)
        .attr("height", height)
        .attr("viewBox", [0, 0, width + margin.left + margin.right, height + margin.top + margin.bottom])
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");   
        
    // Perser les données du fichier
    var data = await d3.csvParse(mesuresTHR.data);
    data.forEach(function (d, i) {
        let date = new Date(mesuresTHR.startDate);
        date.setSeconds(date.getSeconds() + i * mesuresTHR.sampleRate);
        d.ts = date;
        d.T = +d.T;
        d.HR = +d.HR;
    });

    xTime.domain(d3.extent(data, d => d.ts));
    endDate = d3.max(data, d => d.ts);
    yTemperature.domain(d3.extent(data, d => d.T));
    yHumidity.domain(d3.extent(data, d => d.HR));

    // Axe horizontal du temps
    svg.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(xTime));

    // Axe vertical températures
    svg.append("g")
        .attr("class", "axisT")
        .attr("transform", "translate(" + width + ", 0)")
        .call(d3.axisRight(yTemperature))
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", "+2em")
        .attr("class", "axisTitle")
        .style("text-anchor", "middle")
        .text("Température (°C)")
        .attr("transform", "translate(10, " + height/2 + ") rotate(-90)")
        ;

    // Axe vertical humidités
    svg.append("g")
        .attr("class", "axisH")
        .call(d3.axisLeft(yHumidity))
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", "-2em")
        .attr("class", "axisTitle")
        .style("text-anchor", "middle")
        .text("Humidité (%)")
        .attr("transform", "translate(-5, " + height/2 + ") rotate(-90)")
        ;

    // Points courbe température
    svg.append('g')
        .selectAll("dot")
        .data(data)
        .enter()
        .append("circle")
        .attr("cx", d => { return xTime(d.ts); })
        .attr("cy", d => { return yTemperature(d.T); })
        .attr("r", 3)
        .attr("class", "dotT")
        ;

    // Chemin courbe température
    svg.append("path")
        .datum(data)
        .attr("class", "pathT")
        .attr("d", lineTemperature);

    // Points courbe humidité
    svg.append('g')
        .selectAll("dot")
        .data(data)
        .enter()
        .append("circle")
        .attr("cx", function (d) { return xTime(d.ts); })
        .attr("cy", function (d) { return yHumidity(d.HR); })
        .attr("r", 3)
        .attr("fill", "steelblue")
        ;

    // Courbe humidités
    svg.append("path")
        .datum(data)
        .attr("class", "pathH")
        .attr("d", lineHumidity);
}

async function displayLogShocks() {
  // Définir les marges, largeur et hauteur de la courbe
  const margin = { top: 20, right: 120, bottom: 30, left: 60 };
  const width = document.getElementById("chart-shocks").offsetWidth;
  const height = 300 - margin.top - margin.bottom;

  // Définir les échelles des différents axes
  const xTime = d3.scaleTime()
      .range([0, width]);

  const yShocks = d3.scaleLinear()
      .range([height, 0]);

    // Ajouter le SVG dans le document et définir ses dimensions et son emplacement
    const svg = d3.select("#chart-shocks").append("svg")
        .attr("id", "svg")
        .attr("width", width)
        .attr("height", height)
        .attr("viewBox", [0, 0, width + margin.left + margin.right, height + margin.top + margin.bottom])
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");   

    // Parser les données du fichier
    let data = await d3.csvParse(chocs.data);
    data.forEach(function (d, i) {
        let date = new Date(d._dt * 1000);
        d.ts = date;
        d.val = 1;
    });

//    xTime.domain([new Date(chocs.startDate), d3.max(data, d => d.ts)]);
    xTime.domain([new Date(chocs.startDate), endDate]);
    yShocks.domain([0, d3.max(data, d => d.val)]);   
 
    // Axe horizontal du temps
    svg.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(xTime));

    // Axe vertical chocs
    svg.append("g")
        .attr("class", "axisS")
        .call(d3.axisLeft(yShocks))
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", "-2em")
        .style("text-anchor", "middle")
        .attr("class", "axisTitle")
        .text("Chocs")
        .attr("transform", "translate(-5, " + height/2 + ") rotate(-90)")
        ;

    // Segments courbe chocs
    svg.append("g")
        .attr("class", "segmentS")
        .selectAll("line")
        .data(data)
        .join("line")
        .attr("x1", d => { return xTime(d.ts); })
        .attr("x2", d => { return xTime(d.ts); })
        .attr("y1", d => { return yShocks(0); })
        .attr("y2", d => { return yShocks(d.val); });    
        
    // Points courbe chocs
    svg.append('g')
        .selectAll("dot")
        .data(data)
        .enter()
        .append("circle")
        .attr("cx", d => { return xTime(d.ts); })
        .attr("cy", d => { return yShocks(d.val); })
        .attr("r", 3)
        .attr("class", "dotS")
        ;

}