/*
 * adxl3xx.h
 *
 * Librairie pour l'accéléromètre ADXL345 et ADXL375.
 *
 * Basée sur la librairie Arduino de Wolfgang (Wolle) Ewald
 * https://wolles-elektronikkiste.de/en/adxl345-the-universal-accelerometer-part-1 (English)
 * (https://github.com/wollewald/ADXL345_WE)
 *
 *  Created on: Dec 21, 2022
 *      Author: C.Defrance
 */
#ifndef ADXL3XX_H_
#define ADXL3XX_H_

#include <stdint.h>
#include <stdbool.h>

/* Definitions */

#define ADXL_TYPE		345 // ou 375
#if ADXL_TYPE == 375
	#define MILLI_G_PER_LSB         49 // 49mg/lsb pour un adxl375. Voir "Scale Factor at XOUT,YOUT,ZOUT" dans table 1 dans la datasheet
	#define UNITS_PER_G           	20.41 // = 1/0.049. correspond à la "sensitivity" dans la datasheet
#elif ADXL_TYPE == 345
	#define MILLI_G_PER_LSB         3.9 // 3.9mg/lsb pour un ADXL345. Voir "Scale Factor at XOUT,YOUT,ZOUT" dans table 1 dans la datasheet
	#define UNITS_PER_G           	256.41 // = 1/0.0039. correspond à la "sensitivity" dans la datasheet
#else
	#warning "DEFINIR LE TYPE d'ADXL (345 ou 375)"
#endif

#define SDO_ALT_PIN_LEVEL	0

#if SDO_ALT_PIN_LEVEL == 1
	#define ADXL3XX_I2C_7BITS_ADDRESS		0x1D
#else
	#define ADXL3XX_I2C_7BITS_ADDRESS		0x53
#endif


#define INT_PIN_1        0x01
#define INT_PIN_2        0x02
#define ADXL3XX_ACT_LOW  0x01
#define ADXL3XX_ACT_HIGH 0x00


#define ADXL3XX_DEVID            0x00
#define ADXL3XX_THRESH_TAP_SHOCK     0x1D
#define ADXL3XX_OFSX             0x1E
#define ADXL3XX_OFSY             0x1F
#define ADXL3XX_OFSZ             0x20
#define ADXL3XX_DUR              0x21
#define ADXL3XX_LATENT           0x22
#define ADXL3XX_WINDOW           0x23
#define ADXL3XX_THRESH_ACT       0x24
#define ADXL3XX_THRESH_INACT     0x25
#define ADXL3XX_TIME_INACT       0x26
#define ADXL3XX_ACT_INACT_CTL    0x27
#define ADXL3XX_TAP_SHOCK_AXES       0x2A
#define ADXL3XX_ACT_TAP_SHOCK_STATUS 0x2B
#define ADXL3XX_BW_RATE          0x2C
#define ADXL3XX_POWER_CTL        0x2D
#define ADXL3XX_INT_ENABLE       0x2E
#define ADXL3XX_INT_MAP          0x2F
#define ADXL3XX_INT_SOURCE       0x30
#define ADXL3XX_DATA_FORMAT      0x31
#define ADXL3XX_DATAX0           0x32
#define ADXL3XX_DATAX1           0x33
#define ADXL3XX_DATAY0           0x34
#define ADXL3XX_DATAY1           0x35
#define ADXL3XX_DATAZ0           0x36
#define ADXL3XX_DATAZ1           0x37
#define ADXL3XX_FIFO_CTL         0x38
#define ADXL3XX_FIFO_STATUS      0x39

/* Register bits : */

/* > */
#define ADXL3XX_SUPPRESS         0x03
#define ADXL3XX_LOW_POWER        0x04

/* > Reg. DATA_FORMAT */
#define	ADXL3XX_RANGE0			(1 << 0)
#define	ADXL3XX_RANGE1 			(1 << 1)
#define	ADXL3XX_JUSTIFY			(1 << 2)
#define	ADXL3XX_FULL_RES		(1 << 3)
#define ADXL3XX_INT_INVERT		(1 << 5)
#define ADXL3XX_SPI				(1 << 6)
#define ADXL3XX_SELF_TEST		(1 << 7)


/* Other */


#define I2C_TIMEOUT				100 /* ms */

typedef enum ADXL3XX_PWR_CTL {
	ADXL3XX_WAKE_UP_0,
	ADXL3XX_WAKE_UP_1,
	ADXL3XX_SLEEP,
	ADXL3XX_MEASURE,
	ADXL3XX_AUTO_SLEEP,
	ADXL3XX_LINK
} adxl3xx_pwrCtl;

typedef enum ADXL3XX_WAKE_UP { // belongs to POWER CTL
	ADXL3XX_WAKE_UP_FREQ_8,
	ADXL3XX_WAKE_UP_FREQ_4,
	ADXL3XX_WAKE_UP_FREQ_2,
	ADXL3XX_WAKE_UP_FREQ_1,
} adxl3xx_wakeUpFreq;

typedef enum ADXL3XX_DATA_RATE {
	ADXL3XX_DATA_RATE_3200 = 0x0F,
	ADXL3XX_DATA_RATE_1600 = 0x0E,
	ADXL3XX_DATA_RATE_800 = 0x0D,
	ADXL3XX_DATA_RATE_400 = 0x0C,
	ADXL3XX_DATA_RATE_200 = 0x0B,
	ADXL3XX_DATA_RATE_100 = 0x0A,
	ADXL3XX_DATA_RATE_50 = 0x09,
	ADXL3XX_DATA_RATE_25 = 0x08,
	ADXL3XX_DATA_RATE_12_5 = 0x07,
	ADXL3XX_DATA_RATE_6_25 = 0x06,
	ADXL3XX_DATA_RATE_3_13 = 0x05,
	ADXL3XX_DATA_RATE_1_56 = 0x04,
	ADXL3XX_DATA_RATE_0_78 = 0x03,
	ADXL3XX_DATA_RATE_0_39 = 0x02,
	ADXL3XX_DATA_RATE_0_20 = 0x01,
	ADXL3XX_DATA_RATE_0_10 = 0x00
} adxl3xx_dataRate;


typedef enum ADXL345_RANGE {
    ADXL345_RANGE_16G          = 0b11,
    ADXL345_RANGE_8G           = 0b10,
    ADXL345_RANGE_4G           = 0b01,
    ADXL345_RANGE_2G           = 0b00
} adxl345_range;

typedef enum ADXL3XX_ORIENTATION {
	FLAT, FLAT_1, XY, XY_1, YX, YX_1
} adxl3xx_orientation;

typedef enum ADXL3XX_INT {
	ADXL3XX_OVERRUN,
	ADXL3XX_WATERMARK,
	ADXL3XX_FREEFALL,
	ADXL3XX_INACTIVITY,
	ADXL3XX_ACTIVITY,
	ADXL3XX_DOUBLE_TAP_SHOCK,
	ADXL3XX_SINGLE_TAP_SHOCK,
	ADXL3XX_DATA_READY
} adxl3xx_int;



typedef enum ADXL3XX_ACT_TAP_SHOCK_SET {
	ADXL3XX_000,
	ADXL3XX_00Z,
	ADXL3XX_0Y0,
	ADXL3XX_0YZ,
	ADXL3XX_X00,
	ADXL3XX_X0Z,
	ADXL3XX_XY0,
	ADXL3XX_XYZ
} adxl3xx_actShockSet;

typedef enum ADXL3XX_DC_AC {
	ADXL3XX_DC_MODE = 0, ADXL3XX_AC_MODE = 0x08
} adxl3xx_dcAcMode;

typedef enum ADXL3XX_WAKE_UP_FREQ {
	ADXL3XX_WUP_FQ_8, ADXL3XX_WUP_FQ_4, ADXL3XX_WUP_FQ_2, ADXL3XX_WUP_FQ_1
} adxl3xx_wUpFreq;

typedef enum ADXL3XX_ACT_TAP_SHOCK {
	ADXL3XX_TAP_SHOCK_Z,
	ADXL3XX_TAP_SHOCK_Y,
	ADXL3XX_TAP_SHOCK_X,
	ADXL3XX_ASLEEP,
	ADXL3XX_ACT_Z,
	ADXL3XX_ACT_Y,
	ADXL3XX_ACT_X
} adxl3xx_actShock;

typedef enum ADXL3XX_FIFO_MODE {
	ADXL3XX_BYPASS, ADXL3XX_FIFO, ADXL3XX_STREAM, ADXL3XX_TRIGGER
} adxl3xx_fifoMode;

typedef enum ADXL3XX_TRIGGER_INT {
	ADXL3XX_TRIGGER_INT_1, ADXL3XX_TRIGGER_INT_2
} adxl3xx_triggerInt;

typedef struct {
	float x;
	float y;
	float z;
} xyzFloat;


/* Basic settings */

bool ADXL3XX_initI2C(I2C_HandleTypeDef * hi2c, uint8_t i2cAddress);
//void setCorrFactors(float xMin, float xMax, float yMin, float yMax, float zMin, float zMax);
void ADXL3XX_setCorrFactors(float xOffset, float yOffset, float zOffset);
void ADXL3XX_setOffset(float xOffset, float yOffset, float zOffset);
void ADXL3XX_setDataRate(adxl3xx_dataRate rate);
adxl3xx_dataRate ADXL3XX_getDataRate();
char * ADXL3XX_getDataRateAsString(char * returnString);
uint8_t ADXL3XX_getPowerCtlReg();
char * ADXL3XX_getRangeAsString(char * returnString);

/* x,y,z results */

xyzFloat ADXL3XX_getRawValues();
xyzFloat ADXL3XX_getCorrectedRawValues();
xyzFloat ADXL3XX_getGValues();
xyzFloat ADXL3XX_getAngles();
xyzFloat ADXL3XX_getCorrAngles();

/* Angles and Orientation */

void ADXL3XX_measureAngleOffsets();
xyzFloat ADXL3XX_getAngleOffsets();
void ADXL3XX_setAngleOffsets(xyzFloat aos);
adxl3xx_orientation ADXL3XX_getOrientation();
char * ADXL3XX_getOrientationAsString(char * returnString);
float ADXL3XX_getPitch();
float ADXL3XX_getRoll();

/* Power, Sleep, Standby */

void ADXL3XX_setMeasureMode(bool measure);
void ADXL3XX_setSleep(bool sleep, adxl3xx_wUpFreq freq);
//void ADXL3XX_setSleep(bool sleep);
void ADXL3XX_setAutoSleep(bool autoSleep, adxl3xx_wUpFreq freq);
//void ADXL3XX_setAutoSleep(bool autoSleep);
bool ADXL3XX_isAsleep();
void ADXL3XX_setLowPower(bool lowpwr);

/* Interrupts */

void ADXL3XX_setInterrupt(adxl3xx_int type, uint8_t pin);
void ADXL3XX_setInterruptPolarity(uint8_t pol);
void ADXL3XX_deleteInterrupt(adxl3xx_int type);
uint8_t ADXL3XX_readAndClearInterrupts();
bool ADXL3XX_checkInterrupt(uint8_t source, adxl3xx_int type);
void ADXL3XX_setLinkBit(bool link);
void ADXL3XX_setFreeFallThresholds(float ffg, float fft);
void ADXL3XX_setActivityParameters(adxl3xx_dcAcMode mode, adxl3xx_actShockSet axes, float threshold);
void ADXL3XX_setInactivityParameters(adxl3xx_dcAcMode mode, adxl3xx_actShockSet axes, float threshold, uint8_t inactTime);
void ADXL3XX_setGeneralShockParameters(adxl3xx_actShockSet axes, float threshold, float duration, float latent);
void ADXL3XX_setAdditionalDoubleShockParameters(bool suppress, float window);
uint8_t ADXL3XX_getActShockStatus();
char * ADXL3XX_getActShockStatusAsString();

/* FIFO */

void ADXL3XX_setFifoParameters(adxl3xx_triggerInt intNumber, uint8_t samples);
void ADXL3XX_setFifoMode(adxl3xx_fifoMode mode);
uint8_t ADXL3XX_getFifoStatus();
void ADXL3XX_resetTrigger();

#endif /* ADXL3XX_H_ */
