/*
 * adxl3xx.c
 *
 * Librairie pour l'accéléromètre ADXL345 et ADXL375.
 *
 * Basée sur la librairie Arduino de Wolfgang (Wolle) Ewald
 * https://wolles-elektronikkiste.de/en/adxl345-the-universal-accelerometer-part-1 (English)
 * (https://github.com/wollewald/ADXL345_WE)
 *
 *  Created on: Dec 21, 2022
 *      Author: C.Defrance
 */
#include <math.h>
#include <string.h>

#include "stm32f4xx_hal.h"

#include "adxl3xx.h"

static bool init();
static HAL_StatusTypeDef writeRegister(uint8_t reg, uint8_t val);
static HAL_StatusTypeDef readRegister8(uint8_t reg);
static HAL_StatusTypeDef readMultipleRegisters(uint8_t reg, uint8_t count, uint8_t *buf);

static I2C_HandleTypeDef * _hi2c;

static uint8_t _i2cAddress;
static uint8_t _regVal;   // intermediate storage of register values
static xyzFloat _offsetVal;
static xyzFloat _angleOffsetVal;
static xyzFloat _corrFact;
static float _rangeFactor;

/************ Basic settings ************/

bool ADXL3XX_initI2C(I2C_HandleTypeDef * hi2c, uint8_t i2cAddress) {
	_hi2c = hi2c;
	_i2cAddress = i2cAddress;

	return init();
}

/*
void ADXL3XX_setCorrFactors(float xMin, float xMax, float yMin, float yMax, float zMin, float zMax){
    _corrFact.x = UNITS_PER_G / (0.5 * (xMax - xMin));
    _corrFact.y = UNITS_PER_G / (0.5 * (yMax - yMin));
    _corrFact.z = UNITS_PER_G / (0.5 * (zMax - zMin));
    _offsetVal.x = (xMax + xMin) * 0.5;
    _offsetVal.y = (yMax + yMin) * 0.5;
    _offsetVal.z = (zMax + zMin) * 0.5;
}
*/
void ADXL3XX_setCorrFactors(float xOffset, float yOffset, float zOffset) {
    _offsetVal.x = xOffset;
    _offsetVal.y = yOffset;
    _offsetVal.z = zOffset;
}

void ADXL3XX_setOffset(float xOffset, float yOffset, float zOffset) {
	int8_t ofsx;
	int8_t ofsy;
	int8_t ofsz;

	ofsx = -(xOffset / 4);
	ofsy = -(yOffset / 4);
	ofsz = -(yOffset / 4);

	writeRegister(ADXL3XX_OFSX, ofsx);
	writeRegister(ADXL3XX_OFSY, ofsy);
	writeRegister(ADXL3XX_OFSZ, ofsz);
}

void ADXL3XX_setDataRate(adxl3xx_dataRate rate){
    _regVal |= readRegister8(ADXL3XX_BW_RATE);
    _regVal &= 0xF0;
    _regVal |= rate;
    writeRegister(ADXL3XX_BW_RATE, _regVal);
}

adxl3xx_dataRate ADXL3XX_getDataRate(){
    return (adxl3xx_dataRate)(readRegister8(ADXL3XX_BW_RATE) & 0x0F);
}

char * ADXL3XX_getDataRateAsString(char * returnString){
    adxl3xx_dataRate dataRate = (adxl3xx_dataRate)(readRegister8(ADXL3XX_BW_RATE) & 0x0F);

    switch(dataRate) {
        case ADXL3XX_DATA_RATE_3200: returnString = "3200 Hz"; break;
        case ADXL3XX_DATA_RATE_1600: returnString = "1600 Hz"; break;
        case ADXL3XX_DATA_RATE_800:  returnString = "800 Hz";  break;
        case ADXL3XX_DATA_RATE_400:  returnString = "400 Hz";  break;
        case ADXL3XX_DATA_RATE_200:  returnString = "200 Hz";  break;
        case ADXL3XX_DATA_RATE_100:  returnString = "100 Hz";  break;
        case ADXL3XX_DATA_RATE_50:   returnString = "50 Hz";   break;
        case ADXL3XX_DATA_RATE_25:   returnString = "25 Hz";   break;
        case ADXL3XX_DATA_RATE_12_5: returnString = "12.5 Hz"; break;
        case ADXL3XX_DATA_RATE_6_25: returnString = "6.25 Hz"; break;
        case ADXL3XX_DATA_RATE_3_13: returnString = "3.13 Hz"; break;
        case ADXL3XX_DATA_RATE_1_56: returnString = "1.56 Hz"; break;
        case ADXL3XX_DATA_RATE_0_78: returnString = "0.78 Hz"; break;
        case ADXL3XX_DATA_RATE_0_39: returnString = "0.39 Hz"; break;
        case ADXL3XX_DATA_RATE_0_20: returnString = "0.20 Hz"; break;
        case ADXL3XX_DATA_RATE_0_10: returnString = "0.10 Hz"; break;
    }

    return returnString;
}

uint8_t ADXL3XX_getPowerCtlReg(){
    return readRegister8(ADXL3XX_POWER_CTL);
}

char * ADXL3XX_getRangeAsString(char * returnString){
    returnString = "200g";
    return returnString;
}

/************ x,y,z results ************/

xyzFloat ADXL3XX_getRawValues(){
    uint8_t rawData[6];
    xyzFloat rawVal = {0.0, 0.0, 0.0};
    readMultipleRegisters(ADXL3XX_DATAX0, 6, rawData);
    rawVal.x = (int16_t)((rawData[1] << 8) | rawData[0]) * 1.0;
    rawVal.y = (int16_t)((rawData[3] << 8) | rawData[2]) * 1.0;
    rawVal.z = (int16_t)((rawData[5] << 8) | rawData[4]) * 1.0;

    return rawVal;
}

xyzFloat ADXL3XX_getCorrectedRawValues(){
    uint8_t rawData[6];
    xyzFloat rawVal = {0.0, 0.0, 0.0};
    readMultipleRegisters(ADXL3XX_DATAX0, 6, rawData);
    int16_t xRaw = (int16_t)(rawData[1] << 8) | rawData[0];
    int16_t yRaw = (int16_t)(rawData[3] << 8) | rawData[2];
    int16_t zRaw = (int16_t)(rawData[5] << 8) | rawData[4];

    rawVal.x = xRaw * 1.0 - (_offsetVal.x / _rangeFactor);
    rawVal.y = yRaw * 1.0 - (_offsetVal.y / _rangeFactor);
    rawVal.z = zRaw * 1.0 - (_offsetVal.z / _rangeFactor);

    return rawVal;
}

xyzFloat ADXL3XX_getGValues(){
    xyzFloat rawVal = ADXL3XX_getCorrectedRawValues();
    xyzFloat gVal = {0.0, 0.0, 0.0};
    gVal.x = rawVal.x * MILLI_G_PER_LSB * _rangeFactor * _corrFact.x / 1000.0;
    gVal.y = rawVal.y * MILLI_G_PER_LSB * _rangeFactor * _corrFact.y / 1000.0;
    gVal.z = rawVal.z * MILLI_G_PER_LSB * _rangeFactor * _corrFact.z / 1000.0;
    return gVal;
}

xyzFloat ADXL3XX_getAngles(){
    xyzFloat gVal = ADXL3XX_getGValues();
    xyzFloat angleVal = {0.0, 0.0, 0.0};
    if(gVal.x > 1){
        gVal.x = 1;
    }
    else if(gVal.x < -1){
        gVal.x = -1;
    }
    angleVal.x = (asin(gVal.x)) * 57.296;

    if(gVal.y > 1){
        gVal.y = 1;
    }
    else if(gVal.y < -1){
        gVal.y = -1;
    }
    angleVal.y = (asin(gVal.y)) * 57.296;

    if(gVal.z > 1){
        gVal.z = 1;
    }
    else if(gVal.z < -1){
        gVal.z = -1;
    }
    angleVal.z = (asin(gVal.z)) * 57.296;

    return angleVal;
}

xyzFloat ADXL3XX_getCorrAngles(){
    xyzFloat corrAnglesVal = ADXL3XX_getAngles();
    corrAnglesVal.x -= _angleOffsetVal.x;
    corrAnglesVal.y -= _angleOffsetVal.y;
    corrAnglesVal.z -= _angleOffsetVal.z;

    return corrAnglesVal;
}

/************ Angles and Orientation ************/

void ADXL3XX_measureAngleOffsets(){
    _angleOffsetVal = ADXL3XX_getAngles();
}

xyzFloat ADXL3XX_getAngleOffsets(){
    return _angleOffsetVal;
}

void ADXL3XX_setAngleOffsets(xyzFloat aos){
    _angleOffsetVal = aos;
}

adxl3xx_orientation ADXL3XX_getOrientation(){
    adxl3xx_orientation orientation = FLAT;
    xyzFloat angleVal = ADXL3XX_getAngles();
    if(fabs(angleVal.x) < 45){      // |x| < 45
        if(fabs(angleVal.y) < 45){      // |y| < 45
            if(angleVal.z > 0){          //  z  > 0
                orientation = FLAT;
            }
            else{                        //  z  < 0
                orientation = FLAT_1;
            }
        }
        else{                         // |y| > 45
            if(angleVal.y > 0){         //  y  > 0
                orientation = XY;
            }
            else{                       //  y  < 0
                orientation = XY_1;
            }
        }
    }
    else{                           // |x| >= 45
        if(angleVal.x > 0){           //  x  >  0
            orientation = YX;
        }
        else{                       //  x  <  0
            orientation = YX_1;
        }
    }
    return orientation;
}

char * ADXL3XX_getOrientationAsString(char * returnString){
    adxl3xx_orientation orientation = ADXL3XX_getOrientation();
    switch(orientation){
        case FLAT:      strcpy(returnString, "z up");   break;
        case FLAT_1:    strcpy(returnString, "z down"); break;
        case XY:        strcpy(returnString, "y up");   break;
        case XY_1:      strcpy(returnString, "y down"); break;
        case YX:        strcpy(returnString, "x up");   break;
        case YX_1:      strcpy(returnString, "x down"); break;
    }
    return returnString;
}

float ADXL3XX_getPitch(){
    xyzFloat gVal = ADXL3XX_getGValues();
    float pitch = (atan2(-gVal.x, sqrt(fabs((gVal.y*gVal.y + gVal.z*gVal.z))))*180.0)/M_PI;
    return pitch;
}

float ADXL3XX_getRoll(){
    xyzFloat gVal = ADXL3XX_getGValues();
    float roll = (atan2(gVal.y, gVal.z)*180.0)/M_PI;
    return roll;
}

/************ Power, Sleep, Standby ************/

void ADXL3XX_setMeasureMode(bool measure){
    _regVal = readRegister8(ADXL3XX_POWER_CTL);
    if(measure){
        _regVal |= (1<<ADXL3XX_MEASURE);
    }
    else{
        _regVal &= ~(1<<ADXL3XX_MEASURE);
    }
    writeRegister(ADXL3XX_POWER_CTL, _regVal);
}

void ADXL3XX_setSleep(bool sleep, adxl3xx_wUpFreq freq){
    _regVal = readRegister8(ADXL3XX_POWER_CTL);
    _regVal &= 0b11111100;
    _regVal |= freq;
    if(sleep){
        _regVal |= (1<<ADXL3XX_SLEEP);
    }
    else{
    	ADXL3XX_setMeasureMode(false);  // it is recommended to enter Stand Mode when clearing the Sleep Bit!
        _regVal &= ~(1<<ADXL3XX_SLEEP);
        _regVal &= ~(1<<ADXL3XX_MEASURE);
    }
    writeRegister(ADXL3XX_POWER_CTL, _regVal);
    if(!sleep){
    	ADXL3XX_setMeasureMode(true);
    }
}


void ADXL3XX_setAutoSleep(bool autoSleep, adxl3xx_wUpFreq freq){
    if(autoSleep){
        ADXL3XX_setLinkBit(true);
    }
    _regVal = readRegister8(ADXL3XX_POWER_CTL);
    _regVal &= 0b11111100;
    _regVal |= freq;
    if(autoSleep){
        _regVal |= (1<<ADXL3XX_AUTO_SLEEP);
    }
    else{
        _regVal &= ~(1<<ADXL3XX_AUTO_SLEEP);
    }
    writeRegister(ADXL3XX_POWER_CTL, _regVal);
}


bool ADXL3XX_isAsleep(){
    return readRegister8(ADXL3XX_ACT_TAP_SHOCK_STATUS) & (1<<ADXL3XX_ASLEEP);
}

void ADXL3XX_setLowPower(bool lowpwr){
    _regVal = readRegister8(ADXL3XX_BW_RATE);
    if(lowpwr){
        _regVal |= (1<<ADXL3XX_LOW_POWER);
    }
    else{
        _regVal &= ~(1<<ADXL3XX_LOW_POWER);
    }
    writeRegister(ADXL3XX_BW_RATE, _regVal);
}

/************ Interrupts ************/


void ADXL3XX_setInterrupt(adxl3xx_int type, uint8_t pin){
    _regVal = readRegister8(ADXL3XX_INT_ENABLE);
    _regVal |= (1<<type);
    writeRegister(ADXL3XX_INT_ENABLE, _regVal);
    _regVal = readRegister8(ADXL3XX_INT_MAP);
    if(pin == INT_PIN_1){
        _regVal &= ~(1<<type);
    }
    else {
        _regVal |= (1<<type);
    }
    writeRegister(ADXL3XX_INT_MAP, _regVal);
}

void ADXL3XX_setInterruptPolarity(uint8_t pol){
    _regVal = readRegister8(ADXL3XX_DATA_FORMAT);
    if(pol == ADXL3XX_ACT_HIGH){
        _regVal &= ~(0b00100000);
    }
    else if(pol == ADXL3XX_ACT_LOW){
        _regVal |= 0b00100000;
    }
    writeRegister(ADXL3XX_DATA_FORMAT, _regVal);
}

void ADXL3XX_deleteInterrupt(adxl3xx_int type){
    _regVal = readRegister8(ADXL3XX_INT_ENABLE);
    _regVal &= ~(1<<type);
    writeRegister(ADXL3XX_INT_ENABLE, _regVal);
}

uint8_t ADXL3XX_readAndClearInterrupts(){
    _regVal = readRegister8(ADXL3XX_INT_SOURCE);
    return _regVal;
}

bool ADXL3XX_checkInterrupt(uint8_t source, adxl3xx_int type){
    source &= (1<<type);
    return source;
}
void ADXL3XX_setLinkBit(bool link){
    _regVal = readRegister8(ADXL3XX_POWER_CTL);
    if(link){
        _regVal |= (1<<ADXL3XX_LINK);
    }
    else{
        _regVal &= ~(1<<ADXL3XX_LINK);
    }
    writeRegister(ADXL3XX_POWER_CTL, _regVal);
}

void ADXL3XX_setActivityParameters(adxl3xx_dcAcMode mode, adxl3xx_actShockSet axes, float threshold){
    _regVal = (uint16_t)(round(threshold / 0.0625));
    if(_regVal<1){
        _regVal = 1;
    }

    writeRegister(ADXL3XX_THRESH_ACT, _regVal);

    _regVal = readRegister8(ADXL3XX_ACT_INACT_CTL);
    _regVal &= 0x0F;
    _regVal |= (uint16_t)(mode) + ((uint16_t)(axes) << 4);
    writeRegister(ADXL3XX_ACT_INACT_CTL, _regVal);
}

void ADXL3XX_setInactivityParameters(adxl3xx_dcAcMode mode, adxl3xx_actShockSet axes, float threshold, uint8_t inactTime){
    _regVal = (uint16_t)(round(threshold / 0.0625));
    if(_regVal<1){
        _regVal = 1;
    }
    writeRegister(ADXL3XX_THRESH_INACT, _regVal);

    _regVal = readRegister8(ADXL3XX_ACT_INACT_CTL);
    _regVal &= 0xF0;
    _regVal |= (uint16_t)(mode) + (uint16_t)(axes);
    writeRegister(ADXL3XX_ACT_INACT_CTL, _regVal);

    writeRegister(ADXL3XX_TIME_INACT, inactTime);
}

void ADXL3XX_setGeneralShockParameters(adxl3xx_actShockSet axes, float threshold, float duration, float latent){
    _regVal = readRegister8(ADXL3XX_TAP_SHOCK_AXES);
    _regVal &= 0b11111000;
    _regVal |= (uint16_t)(axes);
    writeRegister(ADXL3XX_TAP_SHOCK_AXES, _regVal);

    _regVal = (uint16_t)(round(threshold / 0.0625));
    if(_regVal<1){
        _regVal = 1;
    }
    writeRegister(ADXL3XX_THRESH_TAP_SHOCK,_regVal);

    _regVal = (uint16_t)(round(duration / 0.625));
    if(_regVal<1){
        _regVal = 1;
    }
    writeRegister(ADXL3XX_DUR, _regVal);

    _regVal = (uint16_t)(round(latent / 1.25));
    if(_regVal<1){
        _regVal = 1;
    }
    writeRegister(ADXL3XX_LATENT, _regVal);
}

void ADXL3XX_setAdditionalDoubleShockParameters(bool suppress, float window){
    _regVal = readRegister8(ADXL3XX_TAP_SHOCK_AXES);
    if(suppress){
        _regVal |= (1<<ADXL3XX_SUPPRESS);
    }
    else{
        _regVal &= ~(1<<ADXL3XX_SUPPRESS);
    }
    writeRegister(ADXL3XX_TAP_SHOCK_AXES, _regVal);

    _regVal = (uint16_t)(round(window / 1.25));
    writeRegister(ADXL3XX_WINDOW, _regVal);
}

uint8_t ADXL3XX_getActShockStatus(){
    return readRegister8(ADXL3XX_ACT_TAP_SHOCK_STATUS);
}

char * ADXL3XX_getActShockStatusAsString(char * returnString){
    uint8_t mask = (readRegister8(ADXL3XX_ACT_INACT_CTL)) & 0b01110000;
    mask |= ((readRegister8(ADXL3XX_TAP_SHOCK_AXES)) & 0b00000111);

    _regVal = readRegister8(ADXL3XX_ACT_TAP_SHOCK_STATUS);
    _regVal &= mask;

    if(_regVal & (1<<ADXL3XX_TAP_SHOCK_Z)) { strcat(returnString, "TAP-Z "); }
    if(_regVal & (1<<ADXL3XX_TAP_SHOCK_Y)) { strcat(returnString, "TAP-Y "); }
    if(_regVal & (1<<ADXL3XX_TAP_SHOCK_X)) { strcat(returnString, "TAP-X "); }
    if(_regVal & (1<<ADXL3XX_ACT_Z)) { strcat(returnString, "ACT-Z "); }
    if(_regVal & (1<<ADXL3XX_ACT_Y)) { strcat(returnString, "ACT-Y "); }
    if(_regVal & (1<<ADXL3XX_ACT_X)) { strcat(returnString, "ACT-X "); }

    return returnString;
}

/************ FIFO ************/

void ADXL3XX_setFifoParameters(adxl3xx_triggerInt intNumber, uint8_t samples){
    _regVal = readRegister8(ADXL3XX_FIFO_CTL);
    _regVal &= 0b11000000;
    _regVal |= (samples-1);
    if(intNumber == ADXL3XX_TRIGGER_INT_2){
        _regVal |= 0x20;
    }
    writeRegister(ADXL3XX_FIFO_CTL, _regVal);
}

void ADXL3XX_setFifoMode(adxl3xx_fifoMode mode){
    _regVal = readRegister8(ADXL3XX_FIFO_CTL);
    _regVal &= 0b00111111;
    _regVal |= (mode<<6);
    writeRegister(ADXL3XX_FIFO_CTL,_regVal);
}

uint8_t ADXL3XX_getFifoStatus(){
    return readRegister8(ADXL3XX_FIFO_STATUS);
}

void ADXL3XX_resetTrigger(){
    ADXL3XX_setFifoMode(ADXL3XX_BYPASS);
    ADXL3XX_setFifoMode(ADXL3XX_TRIGGER);
}


/************************************************
    private functions
*************************************************/

bool init(){
	readRegister8(ADXL3XX_DEVID);
    writeRegister(ADXL3XX_POWER_CTL,0);
    writeRegister(ADXL3XX_POWER_CTL, 16);
    ADXL3XX_setMeasureMode(true);
    _rangeFactor = 1.0;
    _corrFact.x = 1.0;
    _corrFact.y = 1.0;
    _corrFact.z = 1.0;
    _offsetVal.x = 0.0;
    _offsetVal.y = 0.0;
    _offsetVal.z = 0.0;
    _angleOffsetVal.x = 0.0;
    _angleOffsetVal.y = 0.0;
    _angleOffsetVal.z = 0.0;
    writeRegister(ADXL3XX_DATA_FORMAT, ADXL3XX_FULL_RES | ADXL3XX_RANGE1 | ADXL3XX_RANGE0);
    writeRegister(ADXL3XX_INT_ENABLE, 0);
    writeRegister(ADXL3XX_INT_MAP,0);
    writeRegister(ADXL3XX_TIME_INACT, 0);
    writeRegister(ADXL3XX_THRESH_INACT,0);
    writeRegister(ADXL3XX_ACT_INACT_CTL, 0);
    writeRegister(ADXL3XX_DUR,0);
    writeRegister(ADXL3XX_LATENT,0);
    writeRegister(ADXL3XX_THRESH_TAP_SHOCK,0);
    writeRegister(ADXL3XX_TAP_SHOCK_AXES,0);
    writeRegister(ADXL3XX_WINDOW, 0);
    ADXL3XX_readAndClearInterrupts();
    writeRegister(ADXL3XX_FIFO_CTL,0);
    writeRegister(ADXL3XX_FIFO_STATUS,0);

    return true;
}

HAL_StatusTypeDef writeRegister(uint8_t reg, uint8_t val){
	uint8_t buf[ 2 ] = {reg, val};
	HAL_StatusTypeDef status = HAL_I2C_Master_Transmit(_hi2c, _i2cAddress, buf, 2, I2C_TIMEOUT);
	return status;
}

uint8_t readRegister8(uint8_t reg){
	uint8_t regValue = 0;
	HAL_I2C_Master_Transmit(_hi2c, _i2cAddress, &reg, 1, I2C_TIMEOUT);
	if( HAL_I2C_Master_Receive(_hi2c, _i2cAddress, &regValue, 1, I2C_TIMEOUT) != HAL_OK) {
		return 0;
	}
	return regValue;
}

HAL_StatusTypeDef readMultipleRegisters(uint8_t reg, uint8_t count, uint8_t *buf){
	HAL_I2C_Master_Transmit(_hi2c, _i2cAddress, &reg, 1, I2C_TIMEOUT);
	return HAL_I2C_Master_Receive(_hi2c, _i2cAddress, buf, count, I2C_TIMEOUT);
}
