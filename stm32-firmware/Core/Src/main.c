/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2023 STMicroelectronics.
 * All rights reserved.
 *
 * This software is licensed under terms that can be found in the LICENSE file
 * in the root directory of this software component.
 * If no LICENSE file comes with this software, it is provided AS-IS.
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "fatfs.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <string.h>
#include <stdarg.h> //for va_list var arg functions
#include <stdbool.h>
#include <time.h>

#include "sht2x.h"
#include "adxl3xx.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define STANDBY_DURATION 10 // Durée de veille en secondes
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;

RTC_HandleTypeDef hrtc;

SD_HandleTypeDef hsd;
DMA_HandleTypeDef hdma_sdio_rx;
DMA_HandleTypeDef hdma_sdio_tx;

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */
bool shock = false;
char trailer[] = "`\n};"; // fin de fichier de log
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_I2C1_Init(void);
static void MX_RTC_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_SDIO_SD_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
// Redéfinition des fonctions de base utilisées par printf() et scanf()
// (->__io_putchar() et __io_getchar()) pour que les entrées/sorties standards
// utilisent l'UART2 du STM32
// Voir https://forum.digikey.com/t/easily-use-scanf-on-stm32/21103(Easily Use scanf on STM32)
#ifdef __GNUC__
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#define GETCHAR_PROTOTYPE int __io_getchar(void)
#else
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#define GETCHAR_PROTOTYPE int fgetc(FILE *f)
#endif

PUTCHAR_PROTOTYPE {
	HAL_UART_Transmit(&huart2, (uint8_t*) &ch, 1, HAL_MAX_DELAY);
	return ch;
}

GETCHAR_PROTOTYPE {
	uint8_t ch = 0;

	/* Clear the Overrun flag just before receiving the first character */
	__HAL_UART_CLEAR_OREFLAG(&huart2);

	/* Wait for reception of a character on the USART RX line and echo this
	 * character on console */
	HAL_UART_Receive(&huart2, (uint8_t*) &ch, 1, HAL_MAX_DELAY);
	HAL_UART_Transmit(&huart2, (uint8_t*) &ch, 1, HAL_MAX_DELAY);
	return ch;
}

/**
 * Ecrit une entrée dans un fichier de log (T/HR ou chocs)
 * @param fileName Nom du fichier de log
 * @param logRecord entrée de log
 */
void writeLog(char * fileName, char * logRecord) {
	uint32_t byteswritten;

	/** Enregistrer l'acquisition dans le log (timestamp + valeurs T/HR) : */

	/** 1/ Monter SD Card (nécessaire après un réveil) */
	if (f_mount(&SDFatFS, (TCHAR const*) SDPath, 0) != FR_OK) {
		printf("f_mount failed !\n");
		Error_Handler();
	}

	/** 2/ Ouvrir fichier de log */
	if (f_open(&SDFile, fileName, FA_OPEN_APPEND | FA_WRITE)
			!= FR_OK) {
		printf("f_open (append) failed !\n");
		Error_Handler();
	}

	/** 3/ Se positionner à la fin du fichier juste avant le trailer */
	FSIZE_t fileSize = f_size(&SDFile);
	f_lseek(&SDFile, fileSize - sizeof(trailer) + 1);

	/** 4/ Ecrire l'enregistrement de log suivi de la fin de fichier*/
	f_write(&SDFile, logRecord, strlen(logRecord), (void *) &byteswritten);
	f_write(&SDFile, trailer, strlen(trailer), (void *) &byteswritten);

	/** 5/ Fermer fichier */
	f_close(&SDFile);

	/** 6/ Démonter SD card */
	f_mount(NULL, (TCHAR const*) NULL, 0);

	return;
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
	/* Désactiver la mise en tampon pour l'entrée standard pour éviter les
	 comportements inattendus lors de l'appel à scanf()
	(Voir https://forum.digikey.com/t/easily-use-scanf-on-stm32/21103(Easily Use scanf on STM32))
	 */
	setvbuf(stdin, NULL, _IONBF, 0);

	/* Déclarer les variables en lien avec l'utilisation de FatFS */
	FRESULT res;
	uint32_t byteswritten;

	/* Déclarer les variables en lien avec l'utilisation de la RTC */
	RTC_TimeTypeDef now = { 0 };
	RTC_DateTypeDef today = { 0 };

	/** Déclarer les autres variables */
	int standbyDuration = STANDBY_DURATION * 32768 / 16; // Durée de veille
	struct tm lastPeriodicWakeUpTime; // date/heure dernier réveil périodique

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_USART2_UART_Init();
  MX_I2C1_Init();
  MX_RTC_Init();
  MX_USART1_UART_Init();
  MX_SDIO_SD_Init();
  MX_FATFS_Init();
  /* USER CODE BEGIN 2 */

	/** Initialiser le SHT21 */
	SHT2x_Init(&hi2c1);

	/** Initialiser l'ADXL : */
	ADXL3XX_initI2C(&hi2c1, ADXL3XX_I2C_7BITS_ADDRESS << 1);
	ADXL3XX_setDataRate(ADXL3XX_DATA_RATE_50);

	/** Configurer la détection de choc :
	   . accélérations de 3.0g pendant au moins 30ms séparées de 100.0ms sur
	     n'importe quel axe
	   . interruption single shock générée sur broche int1_pin
	   . polarité Interruption = état haut
	   . Ajuste les facteurs de correction de l'accéléromètre à partir de
	     valeurs mesurées expérimentalement
	 */
	ADXL3XX_setGeneralShockParameters(ADXL3XX_XYZ, 3.0, 30, 100.0);
	ADXL3XX_setInterrupt(ADXL3XX_SINGLE_TAP_SHOCK, INT_PIN_1);
	ADXL3XX_setInterruptPolarity(ADXL3XX_ACT_HIGH);
	ADXL3XX_setCorrFactors(12.181, 11.348, 11.936);

	/** Initialiser la RTC avec la date/heure saisies par l'utilisateur */
	int DD, MM, YY, WD, hh, mm, ss;

	printf("\n\nSaisir la date avec le format jj/mm/aa : ");
	scanf("%x/%x/%x", &DD, &MM, &YY);
	today.Date = DD;
	today.Month = MM;
	today.Year = YY;

	printf("\nSaisir le jour de la semaine (0=Lundi -> 6=Dimanche) : ");
	scanf("%d", &WD);
	today.WeekDay = WD;

	printf("\nSaisir l'heure avec le format hh:mm:ss : ");
	scanf("%x:%x:%x", &hh, &mm, &ss);
	now.Hours = hh;
	now.Minutes = mm;
	now.Seconds = ss;

	if (HAL_RTC_SetTime(&hrtc, &now, RTC_FORMAT_BCD) != HAL_OK) {
		Error_Handler();
	}
	if (HAL_RTC_SetDate(&hrtc, &today, RTC_FORMAT_BCD) != HAL_OK) {
		Error_Handler();
	}

	/** Créer les fichiers de log sur la SD Card (T/HR + Chocs) */
	if (f_mount(&SDFatFS, (TCHAR const*) SDPath, 0) != FR_OK) {
		printf("f_mount failed !\n");
		Error_Handler();
	} else {
		// Ouvrir le fichier de log de T/HR en écriture et mode création
		// (pour le réinitialiser)
		if (f_open(&SDFile, "THR-LOG.JS", FA_CREATE_ALWAYS | FA_WRITE) != FR_OK) {
			printf("f_open (create) failed !\n");
			Error_Handler();
		} else {
			// Initialiser le contenu initial du fichier avec :
			// . l'heure de début de la campagne de mesure (-> startdate)
			// . la fréquence d'échantillonnage (-> sampleRate)
			// . la 1ère ligne du CSV où seront écrites les acquisitions (-> data)
			// . la fin de fichier (-> "`\n};")
			char wtext[] = "var mesuresTHR = {\n"
					"'startDate': YYYY-MM-DDThh:mm:ss.000Z,\n"
					"'sampleRate' : 1800,\n"
					"'data': `T,HR`\n};";
			sprintf(wtext, "var mesuresTHR = {\n"
					"'startDate': \"20%02x-%02x-%02xT%02x:%02x:%02x.000Z\",\n" // !! date/heure en BCD !!
					"'sampleRate' : 10,\n"
					"'data': `T,HR`\n};", YY, MM, DD, hh, mm, ss);
			res = f_write(&SDFile, wtext, strlen(wtext), (void*) &byteswritten);
			if ((byteswritten == 0) || (res != FR_OK)) {
				printf("f_write failed !\n");
				Error_Handler();
			} else {
				f_close(&SDFile);
			}
		}

		// Ouvrir le fichier de log de chocs en écriture et mode création
		// (pour le réinitialiser)
		if (f_open(&SDFile, "G-LOG.JS", FA_CREATE_ALWAYS | FA_WRITE) != FR_OK) {
			printf("f_open (create) failed !\n");
			Error_Handler();
		} else {
			// Initialiser le contenu initial du fichier avec :
			// . l'heure de début de la campagne de mesure (-> startdate)
			// . la fréquence d'échantillonnage (-> sampleRate)
			// . la 1ère ligne du CSV où seront écrites les acquisitions (-> data)
			// . la fin de fichier (-> "`\n};")
			char wtext[] = "var chocs = {\n"
					"'startDate': \"YYYY-MM-DDThh:mm:ss.000Z\",\n"
					"'data': `_dt`\n};";
			sprintf(wtext, "var chocs = {\n"
					"'startDate': \"20%02x-%02x-%02xT%02x:%02x:%02x.000Z\",\n" // !! date/heure en BCD !!
					"'data': `_dt`\n};", YY, MM, DD, hh, mm, ss);
			res = f_write(&SDFile, wtext, strlen(wtext), (void*) &byteswritten);
			if ((byteswritten == 0) || (res != FR_OK)) {
				printf("f_write failed !\n");
				Error_Handler();
			} else {
				f_close(&SDFile);
			}
		}

	}
	// Re-monter la SD Card pour re-initialiser le système de fichiers
	f_mount(&SDFatFS, (TCHAR const*) NULL, 0);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	printf("\n\nLet's go !\r\n\n");
	while (1) {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

		/** Lire heure/date (obligatoirement dans cet ordre) pour horodater l'évènement */
		if (HAL_RTC_GetTime(&hrtc, &now, RTC_FORMAT_BIN) != HAL_OK) {
			Error_Handler();
		}
		if (HAL_RTC_GetDate(&hrtc, &today, RTC_FORMAT_BIN) != HAL_OK) {
			Error_Handler();
		}

		/** SI réveil dû à un choc ALORS */
		if (shock) {
			/** Afficher l'origine/nature du choc */
			char shockAxes[32] = "";
			ADXL3XX_getActShockStatusAsString(shockAxes);
			uint8_t intSource = ADXL3XX_readAndClearInterrupts();
			if (ADXL3XX_checkInterrupt(intSource, ADXL3XX_SINGLE_TAP_SHOCK)) {
				printf("\r\n!! Choc %s !! (@%02d:%02d:%02d)\r\n\n", shockAxes,
						now.Hours, now.Minutes, now.Seconds);
			}

			/** Ajuster la durée de passage en mode basse consommation pour maintenir
			    un réveil périodique toutes les STANDBY_DURATION secondes même si des chocs
			    surviennent entre temps */
			time_t tsNow;
			struct tm tmNow;
			time_t tsLastPeriodicWakeUpTime;

			tmNow.tm_hour = now.Hours;
			tmNow.tm_min = now.Minutes;
			tmNow.tm_sec = now.Seconds;
			tmNow.tm_mday = today.Date;
			tmNow.tm_mon = today.Month - 1;
			tmNow.tm_year = today.Year + 100; // tm_year indique une année depuis 1900

			tsNow = mktime(&tmNow);
			tsLastPeriodicWakeUpTime = mktime(&lastPeriodicWakeUpTime);

			time_t deltaT = difftime(tsNow, tsLastPeriodicWakeUpTime);
			printf("deltaT = %d\n", (int) deltaT);

			standbyDuration = 32768 / 16 * (STANDBY_DURATION - deltaT);
			printf("Nouvelle durée de standby : %d \n"
					, (int) (STANDBY_DURATION - deltaT)
			);

			/** Enregistrer l'acquisition dans le log (timestamp + valeurs T/HR) : */
			char logEntry[128];
			sprintf(logEntry, "\n%lu", (long unsigned int)(tsNow));
			writeLog("G-LOG.JS", logEntry);

			/** Signaler le choc comme traité */
			shock = false;

			/** SINON (réveil périodique) */
		} else {
			/** Mémoriser date/heure du réveil
			    (pour ajuster si besoin la durée d'endormissement
			    en cas de futurs réveils par choc */
			lastPeriodicWakeUpTime.tm_hour = now.Hours;
			lastPeriodicWakeUpTime.tm_min = now.Minutes;
			lastPeriodicWakeUpTime.tm_sec = now.Seconds;
			lastPeriodicWakeUpTime.tm_mday = today.Date;
			lastPeriodicWakeUpTime.tm_mon = today.Month - 1; // tm_mon débute à 0 pour janvier et non à 1 comme dans la RTC
			lastPeriodicWakeUpTime.tm_year = today.Year + 100; // tm_year indique une année depuis 1900 et pas depuis 2000

			/** Acquérir T/HR */
			float tC = SHT2x_GetTemperature(SHT2x_HOLD_MASTER);
			float rh = SHT2x_GetRelativeHumidity(SHT2x_HOLD_MASTER);

			/** Acquérir valeur courante accéléromètre (pour info seulement) */
			xyzFloat g = ADXL3XX_getGValues();

			/** Afficher les valeurs acquises */
			printf("%02d/%02d/20%02d @ %02d:%02d:%02d :\n\r", today.Date,
					today.Month, today.Year, now.Hours, now.Minutes, now.Seconds

			);
			printf("\tTcelsius : %+05.2f / HR%% : %+3.0f / G(x:%g, y:%g, z:%g)\r\n",
					tC, rh, g.x, g.y, g.z);

			/** Enregistrer l'acquisition dans le log (timestamp + valeurs T/HR) : */
			char logEntry[128];
			sprintf(logEntry, "\n%+04.1f,%02.0f", tC, rh);
			writeLog("THR-LOG.JS", logEntry);

			/** Définir la durée de passage en mode basse consommation
			    (utile lorsque le dernier réveil a été déclenché par un choc) */
			standbyDuration = STANDBY_DURATION * 32768 / 16;
		}
		/** FINSI réveil choc ou périodique */

#if 1
		/** Préparer l'entrée dans le mode STOP : */
		/** 1/ Suspendre l'interruption liée au "tick" (utilisée par HAL_Delay())
		        pour éviter le réveil toutes les ms */
		HAL_SuspendTick();

		/** 2/ Programmer la durée de veille */
		HAL_RTCEx_SetWakeUpTimer_IT(&hrtc, standbyDuration, RTC_WAKEUPCLOCK_RTCCLK_DIV16);

		/** Entrer dans le mode STOP (~305µA mesurée sur nucléo) */
		HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON, PWR_STOPENTRY_WFI);

		//////////////////////////////////////////////////////////
		///  ICI LE MCU EST EN MODE STOP                       ///
		/// (il reprendra son exécution à la ligne suivante)  ///
		//////////////////////////////////////////////////////////

		/** Sortir du mode STOP : */
		/** 1/ Désactiver le timer de sortie du mode STOP */
		HAL_RTCEx_DeactivateWakeUpTimer(&hrtc);
		/** 2/ Reconfigurer les horloges (qui ont été arrétées lors du STOP) */
		SystemClock_Config();
		/** 3/ Relancer le tick */
		HAL_ResumeTick();
#else
		HAL_Delay(Delay)5000);
#endif
	}
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSE;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 72;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 3;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief RTC Initialization Function
  * @param None
  * @retval None
  */
static void MX_RTC_Init(void)
{

  /* USER CODE BEGIN RTC_Init 0 */

  /* USER CODE END RTC_Init 0 */

  RTC_TimeTypeDef sTime = {0};
  RTC_DateTypeDef sDate = {0};

  /* USER CODE BEGIN RTC_Init 1 */

  /* USER CODE END RTC_Init 1 */

  /** Initialize RTC Only
  */
  hrtc.Instance = RTC;
  hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
  hrtc.Init.AsynchPrediv = 127;
  hrtc.Init.SynchPrediv = 255;
  hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
  hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
  if (HAL_RTC_Init(&hrtc) != HAL_OK)
  {
    Error_Handler();
  }

  /* USER CODE BEGIN Check_RTC_BKUP */

  /* USER CODE END Check_RTC_BKUP */

  /** Initialize RTC and set the Time and Date
  */
  sTime.Hours = 0x0;
  sTime.Minutes = 0x0;
  sTime.Seconds = 0x0;
  sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sTime.StoreOperation = RTC_STOREOPERATION_RESET;
  if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  }
  sDate.WeekDay = RTC_WEEKDAY_FRIDAY;
  sDate.Month = RTC_MONTH_FEBRUARY;
  sDate.Date = 0x24;
  sDate.Year = 0x23;

  if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  }

  /** Enable the WakeUp
  */
  if (HAL_RTCEx_SetWakeUpTimer_IT(&hrtc, 0, RTC_WAKEUPCLOCK_RTCCLK_DIV16) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN RTC_Init 2 */

  /* USER CODE END RTC_Init 2 */

}

/**
  * @brief SDIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_SDIO_SD_Init(void)
{

  /* USER CODE BEGIN SDIO_Init 0 */

  /* USER CODE END SDIO_Init 0 */

  /* USER CODE BEGIN SDIO_Init 1 */

  /* USER CODE END SDIO_Init 1 */
  hsd.Instance = SDIO;
  hsd.Init.ClockEdge = SDIO_CLOCK_EDGE_RISING;
  hsd.Init.ClockBypass = SDIO_CLOCK_BYPASS_DISABLE;
  hsd.Init.ClockPowerSave = SDIO_CLOCK_POWER_SAVE_DISABLE;
  hsd.Init.BusWide = SDIO_BUS_WIDE_1B;
  hsd.Init.HardwareFlowControl = SDIO_HARDWARE_FLOW_CONTROL_DISABLE;
  hsd.Init.ClockDiv = 0;
  /* USER CODE BEGIN SDIO_Init 2 */

  /* USER CODE END SDIO_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 9600;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 9600;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA2_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA2_Stream3_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Stream3_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA2_Stream3_IRQn);
  /* DMA2_Stream6_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Stream6_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA2_Stream6_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, RED_RGB_Pin|GREEN_RGB_Pin|BLUE_RGB_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : B1_Pin */
  GPIO_InitStruct.Pin = B1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : WakeUp_Pin */
  GPIO_InitStruct.Pin = WakeUp_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(WakeUp_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : VBUS_EXTI1_Pin */
  GPIO_InitStruct.Pin = VBUS_EXTI1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(VBUS_EXTI1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : SW_USER_Pin */
  GPIO_InitStruct.Pin = SW_USER_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(SW_USER_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : SPI_SCK_FRAM_Pin SPI1_MOSI_FRAM_Pin */
  GPIO_InitStruct.Pin = SPI_SCK_FRAM_Pin|SPI1_MOSI_FRAM_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF5_SPI1;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : SD_DETECT_Pin */
  GPIO_InitStruct.Pin = SD_DETECT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(SD_DETECT_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : RED_RGB_Pin GREEN_RGB_Pin BLUE_RGB_Pin */
  GPIO_InitStruct.Pin = RED_RGB_Pin|GREEN_RGB_Pin|BLUE_RGB_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : PA11 PA12 */
  GPIO_InitStruct.Pin = GPIO_PIN_11|GPIO_PIN_12;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF10_OTG_FS;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI0_IRQn);

}

/* USER CODE BEGIN 4 */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {
	if (GPIO_Pin == WakeUp_Pin) {
		shock = true;
	}
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	__disable_irq();
	while (1) {
	}
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
